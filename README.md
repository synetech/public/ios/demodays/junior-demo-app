Demo day application
========================

 Description
------
- very simple ***Swift / iOS*** project
- based on **architecture chosen by you**
- basic **API (REST)** interaction

### Resources

You can find the design and all resources in [the Figma file](https://www.figma.com/file/aUQJ70k3l1TXHwEkkBB8ik/Demo-Day-iOS).


Tasks
------

### 1. Design the first screen according to the *iOS_list.png* image.
- layout screen according to the image
- design custom TableViewCell that will contain transaction image, description, type, amount and arrow
- download data for the "SYNETECH s.r.o." from REST API -> https://stub.bbeight.synetech.cz/v1/customers/{id}
    - You can find "SYNETECH s.r.o." id here [https://stub.bbeight.synetech.cz/v1/customers)](https://stub.bbeight.synetech.cz/v1/customers/)
- convert transactions to some more useful custom model that you can use to fill in custom items in list


### 2. Design the transaction detail screen according to the other image.
- layout screen according to the image
- change the image depending on the type of the transaction
- create a *like/favourite* functionality


### 3. Make it so that when user taps transaction in the table view it shows him its details
- use proper transition


### 4. [OPTIONAL] Add pull to refresh functionality 
- Pulling down on the list refreshes data


### 5. [OPTIONAL] Add notifications to the application
- register for custom notification
- liking sends notification that shows pop up with text "You narcistic piece of shit."
